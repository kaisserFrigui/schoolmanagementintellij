package com.cifop.schoolmngt.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.cifop.schoolmngt.entities.ClassRoom;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassroomRepository extends JpaRepository<ClassRoom, Long> {

}
