package com.cifop.schoolmngt.services;

import com.cifop.schoolmngt.entities.ClassRoom;

import java.util.List;

public interface ClassroomService {

    ClassRoom saveClassroom(ClassRoom classRoom);

    List<ClassRoom> fetchClassroomList();

    ClassRoom updateClassroom(ClassRoom classRoom, Long id);

    void deleteClassroomById(Long classroomId);

}
