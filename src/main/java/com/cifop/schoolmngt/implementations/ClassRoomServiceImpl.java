package com.cifop.schoolmngt.implementations;

import com.cifop.schoolmngt.entities.ClassRoom;
import com.cifop.schoolmngt.repositories.ClassroomRepository;
import com.cifop.schoolmngt.services.ClassroomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ClassRoomServiceImpl implements ClassroomService {
    @Autowired
    private ClassroomRepository classroomRepository;

    @Override
    public ClassRoom saveClassroom(ClassRoom classRoom) {
        return classroomRepository.save(classRoom);
    }

    @Override
    public List<ClassRoom> fetchClassroomList() {
        return classroomRepository.findAll();
    }

    @Override
    public ClassRoom updateClassroom(ClassRoom newclassRoom, Long id) {
        return classroomRepository.findById(id).map(classRoom -> {
            classRoom.setGrade(newclassRoom.getGrade());
            classRoom.setName(newclassRoom.getName());
            return classroomRepository.save(classRoom);
        }).orElseGet(() -> {
            newclassRoom.setId(id);
            return classroomRepository.save(newclassRoom);
        });
    }

    @Override
    public void deleteClassroomById(Long classroomId) {
        classroomRepository.deleteById(classroomId);
    }
}
