package com.cifop.schoolmngt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchoolMngtApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchoolMngtApplication.class, args);
	}

}
