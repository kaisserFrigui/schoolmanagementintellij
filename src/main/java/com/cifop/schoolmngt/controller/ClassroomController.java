package com.cifop.schoolmngt.controller;

import com.cifop.schoolmngt.entities.ClassRoom;
import com.cifop.schoolmngt.implementations.ClassRoomServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ClassroomController {

    @Autowired
    private ClassRoomServiceImpl classRoomServiceImpl;

    @GetMapping("/classrooms")
    public List<ClassRoom>findAllClassrooms(){
        return  classRoomServiceImpl.fetchClassroomList();
    }
    @PostMapping("/classrooms")
    public ClassRoom saveClassroom(@RequestBody ClassRoom classRoom){
        return classRoomServiceImpl.saveClassroom(classRoom);
    }
    @DeleteMapping("/classrooms/{id}")
    public void deleteClassroom(@PathVariable(name = "id") Long id){
        classRoomServiceImpl.deleteClassroomById(id);
    }
    @PutMapping("/classrooms/{id}")
    public ClassRoom updateClassroom(@RequestBody ClassRoom newclassRoom, @PathVariable(name = "id") Long id){
       return  classRoomServiceImpl.updateClassroom(newclassRoom,id);
    }

}
