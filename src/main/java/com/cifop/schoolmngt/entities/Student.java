package com.cifop.schoolmngt.entities;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "Student")
@NamedQuery(name = "FindAllStudents",query = "SELECT s from Student s")
@NamedQuery(name = "FindStudentByFirstNameQuery",query = "SELECT s from Student s where s.firstName Like :firstName")
//OnetoMany ManytoOne
//@NamedQuery(name="FindStudentByClassroom",query = "SELECT c from classroom c join c.students s where s.id= :idStudent")
public class Student {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String firstName;
	private String lastName;
	@Column(name = "address")
	private String adress;
	private Date dateOfBirth;
	@ManyToOne
	private ClassRoom classroom;
	public Student(String firstName, String lastName,Date dateOfBirth, String adress ) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.adress = adress;
		this.dateOfBirth = dateOfBirth;
	}
	

}
