package com.cifop.schoolmngt.entities;

import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "Class")
@Builder
public class ClassRoom {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long Id;
	private String grade;
	private String name;
	@OneToMany(fetch = FetchType.EAGER)
	private List<Student> student;
	

	


}
