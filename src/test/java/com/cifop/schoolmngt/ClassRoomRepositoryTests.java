package com.cifop.schoolmngt;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.cifop.schoolmngt.entities.Student;
import com.cifop.schoolmngt.repositories.ClassroomRepository;

import java.util.List;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


import java.util.ArrayList;
import java.util.Date;


@ExtendWith(MockitoExtension.class)
public class ClassRoomRepositoryTests {
	
	@Mock
	ClassroomRepository classRoomRepsoitory;
	
	@Test
	public void shouldReturnListOfStudents_WithClassRoomName_WhenDataIsNotEmpty()
	{
		//Arrange
		String classRoomName="A1";
		List<Student> students=new ArrayList<>();
		students.add(new Student( "Ahmed", "ben ali", new Date("12/02/2000"), "Ariana"));
	     students.add(new Student( "Ali", "ben ali", new Date("10/04/1990"), "Ben arous"));
		
		//mock the result of return when the function findStudentsByClassRoomName is called
		when(classRoomRepsoitory.findStudentsByClassRoomName(classRoomName)).thenReturn(students);
		
		//act
		List<Student> actualStudents=classRoomRepsoitory.findStudentsByClassRoomName(classRoomName);
		
		//assert
		assertEquals(students, actualStudents);
		
		//verify that findStudentsByClassRoomName is called one time
        verify(this.classRoomRepsoitory,times(1)).findStudentsByClassRoomName(classRoomName);
		
	}



}
